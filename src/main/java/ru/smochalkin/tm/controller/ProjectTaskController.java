package ru.smochalkin.tm.controller;

import ru.smochalkin.tm.api.controller.IProjectTaskController;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskByProjectId(){
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectId(projectId)) {
            System.out.println("Project not found.");
            return;
        }
        System.out.print("Enter task id: ");
        String taskId = TerminalUtil.nextLine();
        if (!projectTaskService.isTaskId(taskId)) {
            System.out.println("Task not found.");
            return;
        }
        projectTaskService.bindTaskByProjectId(projectId, taskId);
    }

    @Override
    public void unbindTaskByProjectId(){
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectId(projectId)) {
            System.out.println("Project not found.");
            return;
        }
        System.out.print("Enter task id: ");
        String taskId = TerminalUtil.nextLine();
        if (!projectTaskService.isTaskId(taskId)) {
            System.out.println("Task not found.");
            return;
        }
        projectTaskService.unbindTaskByProjectId(projectId, taskId);
    }

    @Override
    public void showTasksByProjectId(){
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectId(projectId)) {
            System.out.println("Project not found.");
            return;
        }
        System.out.println("[TASK LIST BY PROJECT ID = "+ projectId + "]");
        List<Task> tasks = projectTaskService.findTasksByProjectId(projectId);
        for (Task task : tasks) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById(){
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectId(projectId)) {
            System.out.println("Project not found.");
            return;
        }
        projectTaskService.removeProjectById(projectId);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName(){
        System.out.print("Enter project name: ");
        String projectName = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectName(projectName)) {
            System.out.println("Project not found.");
            return;
        }
        projectTaskService.removeProjectByName(projectName);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex(){
        System.out.print("Enter project index: ");
        Integer index = TerminalUtil.nextInt();
        if (!projectTaskService.isProjectIndex(index)) {
            System.out.println("Project not found.");
            return;
        }
        projectTaskService.removeProjectByIndex(index);
        System.out.println("[OK]");
    }

}
