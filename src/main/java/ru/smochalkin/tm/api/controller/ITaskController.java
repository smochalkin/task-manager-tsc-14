package ru.smochalkin.tm.api.controller;

import ru.smochalkin.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showById();

    void showByName();

    void showByIndex();

    void showTask(Task project);

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateById();

    void updateByIndex();

    void startById();

    void startByName();

    void startByIndex();

    void completeById();

    void completeByName();

    void completeByIndex();

    void updateStatusById();

    void updateStatusByName();

    void updateStatusByIndex();

}
