package ru.smochalkin.tm.api.model;

import java.util.Date;

public interface IHasEndDate {

    Date getEndDate();

    void setEndDate(Date name);

}
