package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements ru.smochalkin.tm.api.service.IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectRepository.findById(projectId) == null) return null;
        if (taskRepository.findById(taskId) == null) return null;
        return taskRepository.bindTaskById(projectId, taskId);
    }

    @Override
    public Task unbindTaskByProjectId(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectRepository.findById(projectId) == null) return null;
        if (taskRepository.findById(taskId) == null) return null;
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public List<Task> findTasksByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findTasksByProjectId(projectId);
    }

    @Override
    public void removeProjectById(String id) {
        if (id == null || id.isEmpty()) return;
        taskRepository.removeTasksByProjectId(id);
        projectRepository.removeById(id);
    }

    @Override
    public void removeProjectByName(String name) {
        if (name == null || name.isEmpty()) return;
        Project project = projectRepository.findByName(name);
        if (project == null) return;
        removeProjectById(project.getId());
    }

    @Override
    public void removeProjectByIndex(Integer index) {
        Project project = projectRepository.findByIndex(index);
        removeProjectById(project.getId());
    }

    @Override
    public boolean isProjectId(String id) {
        return projectRepository.findById(id) != null;
    }

    @Override
    public boolean isProjectName(String name) {
        return projectRepository.findByName(name) != null;
    }

    @Override
    public boolean isProjectIndex(Integer index) {
        if (index == null || index < 0) return false;
        if (index >= projectRepository.getCount()) return false;
        return true;
    }

    @Override
    public boolean isTaskId(String id) {
        return taskRepository.findById(id) != null;
    }

}
