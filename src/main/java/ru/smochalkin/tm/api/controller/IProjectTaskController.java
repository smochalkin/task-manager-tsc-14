package ru.smochalkin.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskByProjectId();

    void unbindTaskByProjectId();

    void showTasksByProjectId();

    void removeProjectById();

    void removeProjectByName();

    void removeProjectByIndex();

}
